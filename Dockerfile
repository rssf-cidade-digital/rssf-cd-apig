from python:3.8.2-buster
copy ./app /app
run apt update
run apt install -y python3-pip
run pip3 install -r /app/requirements.txt
workdir /app
entrypoint ["python3", "app.py"]