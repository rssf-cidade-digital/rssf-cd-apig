import matplotlib, os, random, os
import matplotlib.pyplot as plt
import Helpers.ExtrairDadosHelper as ExtrairDadosHelper

pastaBase = "png"
tamanhoFonte=18
tamanhoFigura = (8,12)
dpi = 128

def GerarGráficos(tipo, dados, caminhoApp):
    GarantirDiretórioCriado(caminhoApp)
    ConfigurarGráfico()
    nomeComDiretório, nome = GerarNomeArquivo(tipo, caminhoApp)
    título, eixo, aparência, x, y = GerarVariáveisGráfico(tipo, dados)

    plt.title(título)
    plt.ylabel(eixo)
    plt.plot(x, y, aparência)
    plt.grid(True)
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.savefig("%s%s" % (caminhoApp, nomeComDiretório))

    return {"Arquivo": nome}, 200

def GerarVariáveisGráfico(tipo, dados):
    título = ""
    eixo = ""
    aparência = ""
    x = []
    y = []

    if(tipo == 1):
        x, y = ExtrairDadosHelper.ExtrairTemperatura(dados)
        título = "Temperatura"
        eixo = "Temperatura [°C]"
        aparência = "r-o"
    
    elif(tipo == 2):
        x, y = ExtrairDadosHelper.ExtrairUmidade(dados)
        título = "Umidade"
        eixo = "Umidade [%]"
        aparência = "b-o"
    
    elif(tipo == 3):
        x, y = ExtrairDadosHelper.ExtrairUV(dados)
        título = "Radição UV"
        eixo = "Radiação UV [UVi]"
        aparência = "g-o"

    elif(tipo == 4):
        x, y = ExtrairDadosHelper.ExtrairGásTóxico(dados)
        título = "Gases Tóxicos"
        eixo = "Concentração [ppm]"
        aparência = "k-o"

    elif(tipo == 5):
        x, y = ExtrairDadosHelper.ExtrairFumaça(dados)
        título = "Fumaça"
        eixo = "Concentração [ppm]"
        aparência = "m-o"

    else:
        x,y = ExtrairDadosHelper.ExtrairFogo(dados)
        título = "Fogo"
        eixo = "Intensidade"
        aparência = "c-o"

    return título, eixo, aparência, x, y

def GerarNomeArquivo(tipo, caminhoApp):
    baseNome = "/%s/" %(pastaBase)
    nome = "curva_%d_" %(tipo)
    fimNome = ".png"

    while(True):
        número = random.randint(0,9)
        nome = "%s%d" %(nome, número)
        if(not(os.path.exists("%s%s%s%s"%(caminhoApp, baseNome, nome, fimNome)))):
            break

    nomeComDiretório = "%s%s%s" % (baseNome, nome, fimNome)
    nome = "%s%s" %(nome, fimNome)

    return nomeComDiretório, nome

def GarantirDiretórioCriado(caminhoApp):
    if(not(os.path.exists("%s/%s" %(caminhoApp, pastaBase)))):
        os.mkdir("%s/%s" %(caminhoApp, pastaBase))

def ConfigurarGráfico():
    matplotlib.rcParams.update({'font.size': tamanhoFonte})
    plt.figure(figsize=tamanhoFigura, dpi=dpi)
    