import sys, os
import Bibliotecas.Gráficos as Gráficos
from flask import Flask, request
from flask_restful import Resource, Api
from flask_sqlalchemy import SQLAlchemy

from DAO.FiltroDAO import FiltroDAO

usuário = os.environ["UsuarioMySQL"]
senha = os.environ["SenhaMySQL"]
servidor = os.environ["ServidorMySQL"]
banco = os.environ["BaseDeDadosMySQL"]

try:
    porta = int(os.environ["Porta"])
except:
    porta = 5000

caminhoApp = os.path.dirname(__file__)

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+pymysql://%s:%s@%s/%s" %(usuário, senha, servidor, banco)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

class Monitoramentos(db.Model):
    __tablename__ = "Monitoramentos"
    Id = db.Column(db.Integer, primary_key=True)
    CélulaId = db.Column(db.Integer)
    RequisiçãoId = db.Column(db.Integer)
    Temperatura = db.Column(db.Float)
    Umidade = db.Column(db.Float)
    GásTóxico = db.Column(db.Float)
    Fumaça = db.Column(db.Float)
    Incêndio = db.Column(db.Float)
    RadiaçãoUV= db.Column(db.Float)
    DataHora = db.Column(db.DateTime)

class MonitoramentosControlador(Resource):
    def post(self, tipo, idCelula):
        filtro = FiltroDAO.GerarAPartirJson(request.get_json())
        dados = Monitoramentos.query.filter((Monitoramentos.DataHora >= filtro.inícioDatetime) & (Monitoramentos.DataHora <= filtro.fimDatetime) & (Monitoramentos.CélulaId == idCelula)).order_by(Monitoramentos.DataHora).all()
        return Gráficos.GerarGráficos(tipo, dados, caminhoApp)

api = Api(app)

api.add_resource(MonitoramentosControlador, "/<int:tipo>/<int:idCelula>")

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=porta)