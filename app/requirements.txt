Flask==1.1.1
Flask_SQLAlchemy==2.4.1
Flask_RESTful==0.3.8
mysqlclient==1.4.6
pymysql==0.9.3
matplotlib==3.2.0
cryptography==2.9