def ExtrairTemperatura(dados):
    x = []
    temperatura = []
    for i in dados:
        x.append(ObterTickX(i))
        temperatura.append(i.Temperatura)

    return x, temperatura


def ExtrairUmidade(dados):
    x = []
    umidade = []
    for i in dados:
        x.append(ObterTickX(i))
        umidade.append(i.Umidade)

    return x, umidade

def ExtrairUV(dados):
    x = []
    uv = []
    for i in dados:
        x.append(ObterTickX(i))
        uv.append(i.RadiaçãoUV)

    return x,uv

def ExtrairGásTóxico(dados):
    x = []
    gás = []
    for i in dados:
        x.append(ObterTickX(i))
        gás.append(i.GásTóxico)
    
    return x, gás

def ExtrairFumaça(dados):
    x = []
    fumaça = []
    for i in dados:
        x.append(ObterTickX(i))
        fumaça.append(i.Fumaça)

    return x, fumaça

def ExtrairFogo(dados):
    x = []
    fogo = []
    for i in dados:
        x.append(ObterTickX(i))
        fogo.append(i.Incêndio)

    return x, fogo

def ObterTickX(dado):
    return dado.DataHora.strftime("%d/%m/%Y - %H:%M")