from datetime import datetime

class FiltroDAO:
    def __init__(self, início, fim):
        self.início = início
        self.fim = fim

        self.inícioDatetime = datetime.strptime(início, "%Y-%m-%d %H:%M:%S")
        self.fimDatetime = datetime.strptime(fim, "%Y-%m-%d %H:%M:%S")

    def RetornarJson(self):
        return {"início": self.início, "fim" : self.fim}
    
    @classmethod
    def GerarAPartirJson(cls, json):
        return cls(json["início"], json["fim"])